#!/usr/bin/env bash

cdr2mask ()
{
   # Number of args to shift, 255..255, first non-255 byte, zeroes
   set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
   [[ $1 -gt 1 ]] && shift ${1} || shift
   echo "${1-0}.${2-0}.${3-0}.${4-0}"
}

cd /boot || exit 1
wget --continue --output-document=/boot/netboot.xyz.lkrn https://boot.netboot.xyz/ipxe/netboot.xyz.lkrn

_Dev="$(ip route list | awk '/^default/ {print $5}')"
_Gateway="$(ip route list | awk '/^default/ {print $3}')"
_IP="$(ip -o -4 addr show "${_Dev}" | awk '{print $4}')"
_Netmask="$(cdr2mask "${_IP#*/}")" 
_IP=${_IP%/*}

cat <<_EOF_ > /boot/netboot.xyz-initrd
#!ipxe
#/boot/netboot.xyz-initrd
imgfree
set net0/ip ${_IP%/*}
set net0/netmask ${_Netmask}
set net0/gateway ${_Gateway}
set dns 208.67.222.222
ifopen net0
chain --autofree https://boot.netboot.xyz
_EOF_

cat <<_EOF_ > /etc/grub.d/09_custom
echo "
menuentry 'netboot.xyz' {
    set root='hd0,msdos1'
    linux16 /netboot.xyz.lkrn
    initrd16 /netboot.xyz-initrd
}
"
_EOF_
chmod +x /etc/grub.d/09_custom

update-grub2 || { grub2-mkconfig -o /boot/grub2/grub.cfg; grub2-set-default 0; }

reboot